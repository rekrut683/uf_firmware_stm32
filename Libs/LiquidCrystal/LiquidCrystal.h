#ifndef _LIQUID_CRYSTAL_H
#define _LIQUID_CRYSTAL_H

#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "../../stmlib/MyLib/platforms.h"
#include "../../stmlib/MyLib/systimer/systimer.h"

#if defined STM32F030
#include "../../stmlib/MyLib/pin/pins.h"
#endif

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

typedef struct Display_Struct
{
	Pins_TypeDef* _rs_pin; // LOW: command.  HIGH: character.
	Pins_TypeDef* _rw_pin; // LOW: write to LCD.  HIGH: read from LCD.
	Pins_TypeDef* _enable_pin; // activated by a HIGH pulse.
	Pins_TypeDef* _data_pins[8];
	uint8_t _displayfunction;
	uint8_t _displaycontrol;
	uint8_t _displaymode;
	uint8_t _initialized;
	uint8_t _numlines;
	uint8_t _row_offsets[4];
} LCD1602;

void LiquidCrystal_Init(LCD1602* lcd, Pins_TypeDef* rs, Pins_TypeDef* enable, Pins_TypeDef* d0, Pins_TypeDef* d1, Pins_TypeDef* d2, Pins_TypeDef* d3);
void LiquidCrystal_Begin(LCD1602* lcd, uint8_t cols, uint8_t rows);

void LiquidCrystal_clear(LCD1602* lcd);
void LiquidCrystal_home(LCD1602* lcd);

void LiquidCrystal_noDisplay(LCD1602* lcd);
void LiquidCrystal_display(LCD1602* lcd);
void LiquidCrystal_noBlink(LCD1602* lcd);
void LiquidCrystal_blink(LCD1602* lcd);
void LiquidCrystal_noCursor(LCD1602* lcd);
void LiquidCrystal_cursor(LCD1602* lcd);
void LiquidCrystal_scrollDisplayLeft(LCD1602* lcd);
void LiquidCrystal_scrollDisplayRight(LCD1602* lcd);
void LiquidCrystal_leftToRight(LCD1602* lcd);
void LiquidCrystal_rightToLeft(LCD1602* lcd);
void LiquidCrystal_autoscroll(LCD1602* lcd);
void LiquidCrystal_noAutoscroll(LCD1602* lcd);

void LiquidCrystal_setRowOffsets(LCD1602* lcd, int row1, int row2, int row3, int row4);
void LiquidCrystal_createChar(LCD1602* lcd, uint8_t, uint8_t[]);
void LiquidCrystal_setCursor(LCD1602* lcd, uint8_t, uint8_t);
unsigned long LiquidCrystal_write(LCD1602* lcd, uint8_t);
void LiquidCrystal_command(LCD1602* lcd, uint8_t);

void LiquidCrystal_send(LCD1602* lcd, uint8_t, uint8_t);
void LiquidCrystal_write4bits(LCD1602* lcd, uint8_t);
void LiquidCrystal_write8bits(LCD1602* lcd, uint8_t);
void LiquidCrystal_pulseEnable(LCD1602* lcd);

unsigned long LiquidCrystal_Print(LCD1602* lcd, const char str[]);
void LiquidCrystal_DelayUs(uint32_t us);






#endif