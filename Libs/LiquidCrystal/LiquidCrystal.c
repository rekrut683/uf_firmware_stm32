#include "LiquidCrystal.h"

void LiquidCrystal_Init(LCD1602* lcd, Pins_TypeDef* rs, Pins_TypeDef* enable, Pins_TypeDef* d0, Pins_TypeDef* d1, Pins_TypeDef* d2, Pins_TypeDef* d3)
{
	//init(1, rs, 255, enable, d0, d1, d2, d3, 0, 0, 0, 0);


	lcd->_rs_pin = rs;
	//_rw_pin = 255;
	lcd->_enable_pin = enable;

	lcd->_data_pins[0] = d0;
	lcd->_data_pins[1] = d1;
	lcd->_data_pins[2] = d2;
	lcd->_data_pins[3] = d3;
	lcd->_data_pins[4] = 0;
	lcd->_data_pins[5] = 0;
	lcd->_data_pins[6] = 0;
	lcd->_data_pins[7] = 0;

	lcd->_displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;

	LiquidCrystal_Begin(lcd, 16, 2);
}

void LiquidCrystal_Begin(LCD1602* lcd, uint8_t cols, uint8_t lines)
{
	if (lines > 1) {
		lcd->_displayfunction |= LCD_2LINE;
	}
	lcd->_numlines = lines;

	LiquidCrystal_setRowOffsets(lcd, 0x00, 0x40, 0x00 + cols, 0x40 + cols);

	// for some 1 line displays you can select a 10 pixel high font
	//if ((dotsize != LCD_5x8DOTS) && (lines == 1)) {
	//	_displayfunction |= LCD_5x10DOTS;
	//}

	Pin_Init(lcd->_rs_pin, OUTPUT_MODE, HIGH_SPEED, PUPD_UP, OTYPE_PUSHPULL);
	//pinMode(_rs_pin, OUTPUT);

	// we can save 1 pin by not using RW. Indicate by passing 255 instead of pin#
	//if (_rw_pin != 255) {
	//	pinMode(_rw_pin, OUTPUT);
	//}

	Pin_Init(lcd->_enable_pin, OUTPUT_MODE, HIGH_SPEED, PUPD_UP, OTYPE_PUSHPULL);
	//pinMode(_enable_pin, OUTPUT);

	// Do these once, instead of every time a character is drawn for speed reasons.
	for (int i = 0; i < ((lcd->_displayfunction & LCD_8BITMODE) ? 8 : 4); ++i)
	{
		Pin_Init(lcd->_data_pins[i], OUTPUT_MODE, HIGH_SPEED, PUPD_UP, OTYPE_PUSHPULL);
		//pinMode(_data_pins[i], OUTPUT);
	}

	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending commands. Arduino can turn on way before 4.5V so we'll wait 50

	LiquidCrystal_DelayUs(50000);
	//delayMicroseconds(50000);

	// Now we pull both RS and R/W low to begin commands

	Pin_Clear(lcd->_rs_pin);
	Pin_Clear(lcd->_enable_pin);
	//digitalWrite(_rs_pin, LOW);
	//digitalWrite(_enable_pin, LOW);
	//if (_rw_pin != 255) {
	//	digitalWrite(_rw_pin, LOW);
	//}

	//put the LCD into 4 bit or 8 bit mode
	if (!(lcd->_displayfunction & LCD_8BITMODE)) {
		// this is according to the hitachi HD44780 datasheet
		// figure 24, pg 46

		// we start in 8bit mode, try to set 4 bit mode
		LiquidCrystal_write4bits(lcd, 0x03);
		LiquidCrystal_DelayUs(4500);
		//delayMicroseconds(4500); // wait min 4.1ms

		// second try
		LiquidCrystal_write4bits(lcd, 0x03);
		LiquidCrystal_DelayUs(4500);
		//delayMicroseconds(4500); // wait min 4.1ms

		// third go!
		LiquidCrystal_write4bits(lcd, 0x03);
		LiquidCrystal_DelayUs(150);
		//delayMicroseconds(150);

		// finally, set to 4-bit interface
		LiquidCrystal_write4bits(lcd, 0x02);
	}
	else {
		// this is according to the hitachi HD44780 datasheet
		// page 45 figure 23

		// Send function set command sequence
		LiquidCrystal_command(lcd, LCD_FUNCTIONSET | lcd->_displayfunction);
		LiquidCrystal_DelayUs(4500);
		//delayMicroseconds(4500);  // wait more than 4.1ms

		// second try
		LiquidCrystal_command(lcd, LCD_FUNCTIONSET | lcd->_displayfunction);
		LiquidCrystal_DelayUs(150);
		//delayMicroseconds(150);

		// third go
		LiquidCrystal_command(lcd, LCD_FUNCTIONSET | lcd->_displayfunction);
	}

	// finally, set # lines, font size, etc.
	LiquidCrystal_command(lcd, LCD_FUNCTIONSET | lcd->_displayfunction);

	// turn the display on with no cursor or blinking default
	lcd->_displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
	LiquidCrystal_display(lcd);

	// clear it off
	LiquidCrystal_clear(lcd);

	// Initialize to default text direction (for romance languages)
	lcd->_displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	// set the entry mode
	LiquidCrystal_command(lcd, LCD_ENTRYMODESET | lcd->_displaymode);
}

void LiquidCrystal_clear(LCD1602* lcd)
{
	LiquidCrystal_command(lcd, LCD_CLEARDISPLAY);  // clear display, set cursor position to zero
	LiquidCrystal_DelayUs(2000);
	//delayMicroseconds(2000);  // this command takes a long time!
}

void LiquidCrystal_home(LCD1602* lcd)
{
	LiquidCrystal_command(lcd, LCD_RETURNHOME);  // set cursor position to zero
	LiquidCrystal_DelayUs(2000);
}

void LiquidCrystal_noDisplay(LCD1602* lcd)
{
	lcd->_displaycontrol &= ~LCD_DISPLAYON;
	LiquidCrystal_command(lcd, LCD_DISPLAYCONTROL | lcd->_displaycontrol);
}

void LiquidCrystal_display(LCD1602* lcd)
{
	lcd->_displaycontrol |= LCD_DISPLAYON;
	LiquidCrystal_command(lcd, LCD_DISPLAYCONTROL | lcd->_displaycontrol);
}

void LiquidCrystal_noBlink(LCD1602* lcd)
{
	lcd->_displaycontrol &= ~LCD_BLINKON;
	LiquidCrystal_command(lcd, LCD_DISPLAYCONTROL | lcd->_displaycontrol);
}

void LiquidCrystal_blink(LCD1602* lcd)
{
	lcd->_displaycontrol |= LCD_BLINKON;
	LiquidCrystal_command(lcd, LCD_DISPLAYCONTROL | lcd->_displaycontrol);
}

void LiquidCrystal_noCursor(LCD1602* lcd)
{
	lcd->_displaycontrol &= ~LCD_CURSORON;
	LiquidCrystal_command(lcd, LCD_DISPLAYCONTROL | lcd->_displaycontrol);
}

void LiquidCrystal_cursor(LCD1602* lcd)
{
	lcd->_displaycontrol |= LCD_CURSORON;
	LiquidCrystal_command(lcd, LCD_DISPLAYCONTROL | lcd->_displaycontrol);
}

void LiquidCrystal_scrollDisplayLeft(LCD1602* lcd)
{
	LiquidCrystal_command(lcd, LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void LiquidCrystal_scrollDisplayRight(LCD1602* lcd)
{
	LiquidCrystal_command(lcd, LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

void LiquidCrystal_leftToRight(LCD1602* lcd)
{
	lcd->_displaymode |= LCD_ENTRYLEFT;
	LiquidCrystal_command(lcd, LCD_ENTRYMODESET | lcd->_displaymode);
}

void LiquidCrystal_rightToLeft(LCD1602* lcd)
{
	lcd->_displaymode &= ~LCD_ENTRYLEFT;
	LiquidCrystal_command(lcd, LCD_ENTRYMODESET | lcd->_displaymode);
}

void LiquidCrystal_autoscroll(LCD1602* lcd)
{
	lcd->_displaymode |= LCD_ENTRYSHIFTINCREMENT;
	LiquidCrystal_command(lcd, LCD_ENTRYMODESET | lcd->_displaymode);
}

void LiquidCrystal_noAutoscroll(LCD1602* lcd)
{
	lcd->_displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
	LiquidCrystal_command(lcd, LCD_ENTRYMODESET | lcd->_displaymode);
}

void LiquidCrystal_setRowOffsets(LCD1602* lcd, int row0, int row1, int row2, int row3)
{
	lcd->_row_offsets[0] = row0;
	lcd->_row_offsets[1] = row1;
	lcd->_row_offsets[2] = row2;
	lcd->_row_offsets[3] = row3;
}

void LiquidCrystal_createChar(LCD1602* lcd, uint8_t location, uint8_t* charmap)
{
	location &= 0x7; // we only have 8 locations 0-7
	LiquidCrystal_command(lcd, LCD_SETCGRAMADDR | (location << 3));
	for (int i = 0; i < 8; i++) {
		LiquidCrystal_write(lcd, charmap[i]);
	}
}

void LiquidCrystal_setCursor(LCD1602* lcd, uint8_t col, uint8_t row)
{
	const unsigned long max_lines = sizeof(lcd->_row_offsets) / sizeof(*(lcd->_row_offsets));
	if (row >= max_lines) {
		row = max_lines - 1;    // we count rows starting w/0
	}
	if (row >= lcd->_numlines) {
		row = lcd->_numlines - 1;    // we count rows starting w/0
	}

	LiquidCrystal_command(lcd, LCD_SETDDRAMADDR | (col + lcd->_row_offsets[row]));
}

unsigned long LiquidCrystal_write(LCD1602* lcd, uint8_t value)
{
	LiquidCrystal_send(lcd, value, 1);
	return 1; // assume sucess
}
void LiquidCrystal_command(LCD1602* lcd, uint8_t value)
{
	LiquidCrystal_send(lcd, value, 0);
}

void LiquidCrystal_send(LCD1602* lcd, uint8_t value, uint8_t mode)
{
	if (mode)
	{
		Pin_Set(lcd->_rs_pin);
	}
	else
	{
		Pin_Clear(lcd->_rs_pin);
	}
	//digitalWrite(_rs_pin, mode);

	// if there is a RW pin indicated, set it low to Write
	//if (_rw_pin != 255) {
	//	digitalWrite(_rw_pin, LOW);
	//}

	if (lcd->_displayfunction & LCD_8BITMODE) {
		LiquidCrystal_write8bits(lcd, value);
	}
	else {
		LiquidCrystal_write4bits(lcd, value >> 4);
		LiquidCrystal_write4bits(lcd, value);
	}
}

void LiquidCrystal_write4bits(LCD1602* lcd, uint8_t value)
{
	for (int i = 0; i < 4; i++) {
		if (((value >> i) & 0x01) == 1)
		{
			Pin_Set(lcd->_data_pins[i]);
		}
		else
		{
			Pin_Clear(lcd->_data_pins[i]);
		}
		//digitalWrite(_data_pins[i], (value >> i) & 0x01);
	}

	LiquidCrystal_pulseEnable(lcd);
}

void LiquidCrystal_write8bits(LCD1602* lcd, uint8_t value)
{
	for (int i = 0; i < 8; i++) {
		if (((value >> i) & 0x01) == 1)
		{
			Pin_Set(lcd->_data_pins[i]);
		}
		else
		{
			Pin_Clear(lcd->_data_pins[i]);
		}
		//digitalWrite(_data_pins[i], (value >> i) & 0x01);
	}

	LiquidCrystal_pulseEnable(lcd);
}

void LiquidCrystal_pulseEnable(LCD1602* lcd)
{
	Pin_Clear(lcd->_enable_pin);
	//digitalWrite(_enable_pin, LOW);
	LiquidCrystal_DelayUs(1);
	//delayMicroseconds(1);
	Pin_Set(lcd->_enable_pin);
	//digitalWrite(_enable_pin, HIGH);
	LiquidCrystal_DelayUs(1);
	//delayMicroseconds(1);    // enable pulse must be >450ns
	Pin_Clear(lcd->_enable_pin);
	//digitalWrite(_enable_pin, LOW);
	LiquidCrystal_DelayUs(100);
	//delayMicroseconds(100);   // commands need > 37us to settle
}

// This will print character string to the LCD
unsigned long LiquidCrystal_Print(LCD1602* lcd, const char str[])
{
	if (str == NULL) return 0;

	const uint8_t* buffer = (const uint8_t*)str;
	unsigned long size = strlen(str);
	unsigned long n = 0;

	while (size--) {
		if (LiquidCrystal_write(lcd, *buffer++)) n++;
		else break;
	}
	return n;
}

void LiquidCrystal_DelayUs(uint32_t us)
{
	uint8_t ticks = SystemCoreClock / 1000000;
	for (int i = 0; i < ticks*us; i++);
}