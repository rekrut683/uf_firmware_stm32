#ifndef _GPIOS_H
#define _GPIOS_H

#include "../platforms.h"

#if defined STM32F030
#include "../../StdPeriph/stm32f0xx/inc/stm32f0xx_gpio.h"
#elif defined STM32F10X_MD
#include "../../StdPeriph/stm32f10x/inc/stm32f10x_gpio.h"
#endif

#include <stdint.h>

#define PORTA		&GPORTA
#define PORTB		&GPORTB
#define PORTC		&GPORTC
#if defined STM32F10X_MD
#define GPIO_AF		&GGPIO_AF
#endif

/*
//	TypeDef Struct of GPIO's
*/
typedef struct GPIOS_Struct
{
	GPIO_TypeDef* 			GPIO;
	uint32_t	 			RCC_Bus;
	volatile uint32_t*		RCC_Register;
	uint8_t					PortSource;
} GPIOS_TypeDef;


/*
//	Public methods for GPIO's
*/
void Port_Enable(GPIOS_TypeDef* gpio);
void Port_Disable(GPIOS_TypeDef* gpio);

/*
// static instances for GPIO's STM32f103/STM32f030
*/
extern GPIOS_TypeDef GPORTA;
extern GPIOS_TypeDef GPORTB;
extern GPIOS_TypeDef GPORTC;
#if defined STM32F10X_MD
extern GPIOS_TypeDef GGPIO_AF;
#endif


#endif