#include "gpioss.h"

void Port_Enable(GPIOS_TypeDef* gpio)
{
	*gpio->RCC_Register |= gpio->RCC_Bus;
}

void Port_Disable(GPIOS_TypeDef* gpio)
{
	*gpio->RCC_Register &= ~gpio->RCC_Bus;
}