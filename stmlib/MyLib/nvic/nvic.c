#include "nvic.h"

volatile uint8_t currentSize = 0;

void NVIC_Add(uint8_t irq)
{
	for (int i = 0; i < currentSize; i++)
	{
		if (irq == NVIC_Irq[i])
		{
			return;
		}
	}
	NVIC_Irq[currentSize] = irq;
	currentSize++;
}

void NVIC_Delete(uint8_t irq)
{
	int8_t pos = -1;
	for (int i = 0; i < currentSize; i++)
	{
		if (NVIC_Irq[i] == irq)
		{
			pos = i;
			break;
		}
	}
	if (pos != -1)
	{
		for (int i = pos; i < currentSize; i++)
		{
			NVIC_Irq[i] = NVIC_Irq[i + 1];
		}
		currentSize--;
	}
}

uint8_t NVIC_Check(uint8_t irq)
{
	for (int i = 0; i < currentSize; i++)
	{
		if (irq == NVIC_Irq[i])
		{
			return 1;
		}
	}
	return 0;
}