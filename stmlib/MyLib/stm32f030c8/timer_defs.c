#include "../timer/timers.h"

extern TIMER_TypeDef GTIMER1 =
{
	.Timer = TIM1,
	.RCC_Bus = RCC_APB2Periph_TIM1,
	.RCC_Register = &RCC->APB2ENR,
	.Prescaler = 1,
	.Period = 0,
	.Mode = COUNTER_UP,
	.Callbacks = {0},
	.Count_callbacks = 0,
	.Type = ADVANCED
};

extern TIMER_TypeDef GTIMER3 =
{
	.Timer = TIM3,
	.RCC_Bus = RCC_APB1Periph_TIM3,
	.RCC_Register = &RCC->APB1ENR,
	.Prescaler = 1,
	.Period = 0,
	.Mode = COUNTER_UP,
	.Callbacks = {0},
	.Count_callbacks = 0,
	.Type = GENERAL
};

extern TIMER_TypeDef GTIMER14 =
{
	.Timer = TIM14,
	.RCC_Bus = RCC_APB1Periph_TIM14,
	.RCC_Register = &RCC->APB1ENR,
	.Prescaler = 1,
	.Period = 0,
	.Mode = COUNTER_UP,
	.Callbacks = {0},
	.Count_callbacks = 0,
	.Type = GENERAL
};

extern TIMER_TypeDef GTIMER15 =
{
	.Timer = TIM15,
	.RCC_Bus = RCC_APB2Periph_TIM15,
	.RCC_Register = &RCC->APB2ENR,
	.Prescaler = 1,
	.Period = 0,
	.Mode = COUNTER_UP,
	.Callbacks = {0},
	.Count_callbacks = 0,
	.Type = GENERAL
};

extern TIMER_TypeDef GTIMER16 =
{
	.Timer = TIM16,
	.RCC_Bus = RCC_APB2Periph_TIM16,
	.RCC_Register = &RCC->APB2ENR,
	.Prescaler = 1,
	.Period = 0,
	.Mode = COUNTER_UP,
	.Callbacks = {0},
	.Count_callbacks = 0,
	.Type = GENERAL
};

extern TIMER_TypeDef GTIMER17 =
{
	.Timer = TIM17,
	.RCC_Bus = RCC_APB2Periph_TIM17,
	.RCC_Register = &RCC->APB2ENR,
	.Prescaler = 1,
	.Period = 0,
	.Mode = COUNTER_UP,
	.Callbacks = {0},
	.Count_callbacks = 0,
	.Type = GENERAL
};