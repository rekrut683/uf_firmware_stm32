#include "../exti/exti.h"

extern Exti_TypeDef GEXTI_LINE0 =
{
	.ExtiLine = EXTI_Line0,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE1 =
{
	.ExtiLine = EXTI_Line1,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE2 =
{
	.ExtiLine = EXTI_Line2,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE3 =
{
	.ExtiLine = EXTI_Line3,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE4 =
{
	.ExtiLine = EXTI_Line4,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE5 =
{
	.ExtiLine = EXTI_Line5,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE6 =
{
	.ExtiLine = EXTI_Line6,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE7 =
{
	.ExtiLine = EXTI_Line7,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE8 =
{
	.ExtiLine = EXTI_Line8,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE9 =
{
	.ExtiLine = EXTI_Line9,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE10 =
{
	.ExtiLine = EXTI_Line10,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE11 =
{
	.ExtiLine = EXTI_Line11,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE12 =
{
	.ExtiLine = EXTI_Line12,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE13 =
{
	.ExtiLine = EXTI_Line13,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE14 =
{
	.ExtiLine = EXTI_Line14,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};

extern Exti_TypeDef GEXTI_LINE15 =
{
	.ExtiLine = EXTI_Line15,
	.Callbacks = {0},
	.Trigger = EXTI_Trigger_Rising_Falling,
	.ExtiMode = EXTI_Mode_Interrupt,
	.CallbackCounter = 0
};