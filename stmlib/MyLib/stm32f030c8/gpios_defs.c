#include "../gpio/gpioss.h"

extern GPIOS_TypeDef GPORTA =
{
	.GPIO = GPIOA,
	.RCC_Bus = RCC_AHBPeriph_GPIOA,
	.RCC_Register = &RCC->AHBENR,
	.PortSource = EXTI_PortSourceGPIOA
};

extern GPIOS_TypeDef GPORTB =
{
	.GPIO = GPIOB,
	.RCC_Bus = RCC_AHBPeriph_GPIOB,
	.RCC_Register = &RCC->AHBENR,
	.PortSource = EXTI_PortSourceGPIOB
};

extern GPIOS_TypeDef GPORTC =
{
	.GPIO = GPIOC,
	.RCC_Bus = RCC_AHBPeriph_GPIOC,
	.RCC_Register = &RCC->AHBENR,
	.PortSource = EXTI_PortSourceGPIOC
};