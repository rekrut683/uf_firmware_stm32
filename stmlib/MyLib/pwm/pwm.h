#ifndef _BLDC_H
#define _BLDC_H

#include "../../CMSIS/stm32f10x.h"

#include "../gpio/gpioss.h"
#include "../timer/timers.h"
#include "../pin/pins.h"

#include <stdint.h>

#define PWM1			&GPWM1
#define PWM2			&GPWM2
#define PWM3			&GPWM3
#define PWM4			&GPWM4

typedef enum PWM_MODE_ENUM
{
	SIMPLE 			= 0,
	COMPLEMENTARY	= 1
} PWM_Mode;

typedef enum CHANNEL_STATE_ENUM
{
	OFF,
	ON
} Channel_State;

typedef enum CHANNEL_MODE_ENUM
{
	PWM_MODE,
	ALWAYS_ACTIVE,
	ALWAYS_INACTIVE
} Channel_Mode;

typedef struct PWM_struct
{
	TIMER_TypeDef*		Timer;
	Pins_TypeDef*		Channel1;
	Pins_TypeDef*		Channel2;
	Pins_TypeDef*		Channel3;
	Pins_TypeDef*		Channel4;
	Pins_TypeDef*		NChannel1;
	Pins_TypeDef*		NChannel2;
	Pins_TypeDef*		NChannel3;
	Pins_TypeDef*		NChannel4;
	Pins_TypeDef*		Break_In;
	PWM_Mode 			Mode;
} PWM_TypeDef;

void PWM_Init(PWM_TypeDef* pwm, PWM_Mode mode, uint32_t prescaler, uint32_t period);
void PWM_SetBDTRFunc(PWM_TypeDef* pwm, uint32_t dead_time, uint8_t break_func);

void PWM_Start(PWM_TypeDef* pwm);
void PWM_Stop(PWM_TypeDef* pwm);

void PWM_SetChannel1_pwm(PWM_TypeDef* pwm);
void PWM_SetChannel1_High(PWM_TypeDef* pwm);
void PWM_SetChannel1_Low(PWM_TypeDef* pwm);
void PWM_SetChannel2_pwm(PWM_TypeDef* pwm);
void PWM_SetChannel2_High(PWM_TypeDef* pwm);
void PWM_SetChannel2_Low(PWM_TypeDef* pwm);
void PWM_SetChannel3_pwm(PWM_TypeDef* pwm);
void PWM_SetChannel3_High(PWM_TypeDef* pwm);
void PWM_SetChannel3_Low(PWM_TypeDef* pwm);

void PWM_Channel1_Enable(PWM_TypeDef* pwm);
void PWM_Channel1_Disable(PWM_TypeDef* pwm);
void PWM_Channel2_Enable(PWM_TypeDef* pwm);
void PWM_Channel2_Disable(PWM_TypeDef* pwm);
void PWM_Channel3_Enable(PWM_TypeDef* pwm);
void PWM_Channel3_Disable(PWM_TypeDef* pwm);

void PWM_NChannel1_Enable(PWM_TypeDef* pwm);
void PWM_NChannel1_Disable(PWM_TypeDef* pwm);
void PWM_NChannel2_Enable(PWM_TypeDef* pwm);
void PWM_NChannel2_Disable(PWM_TypeDef* pwm);
void PWM_NChannel3_Enable(PWM_TypeDef* pwm);
void PWM_NChannel3_Disable(PWM_TypeDef* pwm);

void PWM_SetPWMValue(PWM_TypeDef* pwm, uint16_t value);
void PWM_SetPWMValue_Channel1(PWM_TypeDef* pwm, uint16_t value);
void PWM_SetPWMValue_Channel2(PWM_TypeDef* pwm, uint16_t value);
void PWM_SetPWMValue_Channel3(PWM_TypeDef* pwm, uint16_t value);

extern PWM_TypeDef GPWM1;
extern PWM_TypeDef GPWM2;
extern PWM_TypeDef GPWM3;
extern PWM_TypeDef GPWM4;

#endif