#include "pwm.h"

void PWM_Init(PWM_TypeDef* pwm, PWM_Mode mode, uint32_t prescaler, uint32_t period)
{
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	pwm->Mode = mode;
	uint32_t ch1 = pwm->Channel1->Pin;
	
	Timer_Enable(pwm->Timer);
	
	Pin_Init(pwm->Channel1, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	Pin_Init(pwm->Channel2, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	Pin_Init(pwm->Channel3, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		Pin_Init(pwm->NChannel1, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
		Pin_Init(pwm->NChannel2, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
		Pin_Init(pwm->NChannel3, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	}

	Timer_InitAdvanced(pwm->Timer, prescaler, period, COUNTER_UP, 0,0);

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	if(pwm->Timer->Type == ADVANCED || pwm->Mode == COMPLEMENTARY)
	{
		TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
	}
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	if(pwm->Timer->Type == ADVANCED || pwm->Mode == COMPLEMENTARY)
	{
		TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
	}
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	if(pwm->Timer->Type == ADVANCED || pwm->Mode == COMPLEMENTARY)
	{
		TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	}
	TIM_OC1Init(pwm->Timer->Timer, &TIM_OCInitStructure);
	TIM_OC2Init(pwm->Timer->Timer, &TIM_OCInitStructure);
	TIM_OC2Init(pwm->Timer->Timer, &TIM_OCInitStructure);
	if (pwm->Timer->Type == ADVANCED || pwm->Mode == COMPLEMENTARY)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_1, TIM_CCxN_Disable);
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_2, TIM_CCxN_Disable);
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_3, TIM_CCxN_Disable);
	}
}

void PWM_SetBDTRFunc(PWM_TypeDef* pwm, uint32_t dead_time, uint8_t break_func)
{
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
		TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
		TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
		TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_OFF;
		TIM_BDTRInitStructure.TIM_DeadTime = dead_time;
		TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;
		if(break_func)
		{
			TIM_BDTRInitStructure.TIM_Break = TIM_Break_Enable;
			TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_Low;
		}
		TIM_BDTRConfig(pwm->Timer->Timer, &TIM_BDTRInitStructure);
	}
}

void PWM_Start(PWM_TypeDef* pwm)
{
	TIM_Cmd(pwm->Timer->Timer, ENABLE);
	TIM_CtrlPWMOutputs(pwm->Timer->Timer, ENABLE);
	//TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_1, TIM_OCMode_PWM1);
	//TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_2, TIM_OCMode_PWM1);
	//TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_3, TIM_OCMode_PWM1);
}

void PWM_Stop(PWM_TypeDef* pwm)
{
	TIM_Cmd(pwm->Timer->Timer, DISABLE);

	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_1, TIM_CCx_Disable);
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_2, TIM_CCx_Disable);
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_3, TIM_CCx_Disable);

	if(pwm->Timer->Type == ADVANCED)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_1, TIM_CCxN_Disable);
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_2, TIM_CCxN_Disable);
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_3, TIM_CCxN_Disable);
	}

	TIM_CtrlPWMOutputs(pwm->Timer->Timer, DISABLE);
}

/*
	Select Mode Channels
*/
void PWM_SetChannel1_pwm(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_1, TIM_OCMode_PWM1);
}

void PWM_SetChannel1_High(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_1, TIM_ForcedAction_Active);
}

void PWM_SetChannel1_Low(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_1, TIM_ForcedAction_InActive);
}

void PWM_SetChannel2_pwm(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_2, TIM_OCMode_PWM1);
}

void PWM_SetChannel2_High(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_2, TIM_ForcedAction_Active);
}

void PWM_SetChannel2_Low(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_2, TIM_ForcedAction_InActive);
}

void PWM_SetChannel3_pwm(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_3, TIM_OCMode_PWM1);
}

void PWM_SetChannel3_High(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_3, TIM_ForcedAction_Active);
}

void PWM_SetChannel3_Low(PWM_TypeDef* pwm)
{
	TIM_SelectOCxM(pwm->Timer->Timer, TIM_Channel_3, TIM_ForcedAction_InActive);
}

/*
	Enable & Disable Channels
*/
void PWM_Channel1_Enable(PWM_TypeDef* pwm)
{
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_1, TIM_CCx_Enable);
}

void PWM_Channel1_Disable(PWM_TypeDef* pwm)
{
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_1, TIM_CCx_Disable);
}

void PWM_Channel2_Enable(PWM_TypeDef* pwm)
{
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_2, TIM_CCx_Enable);
}

void PWM_Channel2_Disable(PWM_TypeDef* pwm)
{
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_2, TIM_CCx_Disable);
}

void PWM_Channel3_Enable(PWM_TypeDef* pwm)
{
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_3, TIM_CCx_Enable);
}

void PWM_Channel3_Disable(PWM_TypeDef* pwm)
{
	TIM_CCxCmd(pwm->Timer->Timer, TIM_Channel_3, TIM_CCx_Disable);
}

/*
	Enable & Disable Complementary N Channels
*/
void PWM_NChannel1_Enable(PWM_TypeDef* pwm)
{
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_1, TIM_CCxN_Enable);
	}
}

void PWM_NChannel1_Disable(PWM_TypeDef* pwm)
{
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_1, TIM_CCxN_Disable);
	}
}

void PWM_NChannel2_Enable(PWM_TypeDef* pwm)
{
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_2, TIM_CCxN_Enable);
	}
}

void PWM_NChannel2_Disable(PWM_TypeDef* pwm)
{
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_2, TIM_CCxN_Disable);
	}
}

void PWM_NChannel3_Enable(PWM_TypeDef* pwm)
{
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_3, TIM_CCxN_Enable);
	}
}

void PWM_NChannel3_Disable(PWM_TypeDef* pwm)
{
	if(pwm->Timer->Type == ADVANCED && pwm->Mode == COMPLEMENTARY)
	{
		TIM_CCxNCmd(pwm->Timer->Timer, TIM_Channel_3, TIM_CCxN_Disable);
	}
}

/*
	Set PWM Value for channels
*/
void PWM_SetPWMValue(PWM_TypeDef* pwm, uint16_t value)
{
	PWM_SetPWMValue_Channel1(pwm, value);
	PWM_SetPWMValue_Channel2(pwm, value);
	PWM_SetPWMValue_Channel3(pwm, value);
}

void PWM_SetPWMValue_Channel1(PWM_TypeDef* pwm, uint16_t value)
{
	pwm->Timer->Timer->CCR1 = value;
}

void PWM_SetPWMValue_Channel2(PWM_TypeDef* pwm, uint16_t value)
{
	pwm->Timer->Timer->CCR2 = value;
}

void PWM_SetPWMValue_Channel3(PWM_TypeDef* pwm, uint16_t value)
{
	pwm->Timer->Timer->CCR3 = value;
}