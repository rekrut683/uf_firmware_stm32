#include "spi.h"

extern Spi_TypeDef GSPI1 =
{
	.Spi			= SPI1,
	.Nss			= A4,
	.Sck			= A5,
	.Mosi			= A7,
	.Miso			= A6,
	.RCC_Bus		= RCC_APB2Periph_SPI1,
	.RCC_Register	= &RCC->APB1ENR
};

extern Spi_TypeDef GSPI2 =
{
	.Spi			= SPI2,
	.Nss			= B12,
	.Sck			= B13,
	.Mosi			= B15,
	.Miso			= B14,
	.RCC_Bus		= RCC_APB1Periph_SPI2,
	.RCC_Register	= &RCC->APB1ENR
};