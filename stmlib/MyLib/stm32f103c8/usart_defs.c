#include "../uart/uart.h"

extern Uart_TypeDef GUSART1 =
{
	.Usart			= USART1,
	.Tx_pin			= A9,
	.Rx_pin			= A10,
	.Ck_pin			= A8,
	.CTS_pin		= A11,
	.RTS_pin		= A12,
	.RCC_Bus		= RCC_APB2Periph_USART1,
	.RCC_Register	= &RCC->APB2ENR,
	.DmaTxChannel	= DMA_CHANNEL4,
	.DmaRxChannel	= DMA_CHANNEL5,
	.UsartTxAddress	= &USART1->DR
};

extern Uart_TypeDef GUSART2 =
{
	.Usart			= USART2,
	.Tx_pin			= A2,
	.Rx_pin			= A3,
	.Ck_pin			= A4,
	.CTS_pin		= A0,
	.RTS_pin		= A1,
	.RCC_Bus		= RCC_APB1Periph_USART2,
	.RCC_Register	= &RCC->APB1ENR,
	.DmaTxChannel	= DMA_CHANNEL7,
	.DmaRxChannel	= DMA_CHANNEL6,
	.UsartTxAddress = &USART2->DR
};

extern Uart_TypeDef GUSART3 =
{
	.Usart			= USART3,
	.Tx_pin			= B10,
	.Rx_pin			= B11,
	.Ck_pin			= B12,
	.CTS_pin		= B13,
	.RTS_pin		= B14,
	.RCC_Bus		= RCC_APB1Periph_USART3,
	.RCC_Register	= &RCC->APB1ENR,
	.DmaTxChannel	= DMA_CHANNEL2,
	.DmaRxChannel	= DMA_CHANNEL3,
	.UsartTxAddress = &USART3->DR
};