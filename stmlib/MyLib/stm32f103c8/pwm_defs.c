#include "pwm.h"

extern PWM_TypeDef GPWM1 = 
{
	.Timer				= TIMER1,
	.Channel1			= A8,
	.Channel2			= A9,
	.Channel3			= A10,
	.Channel4			= A11,
	.NChannel1			= B13,
	.NChannel2			= B14,
	.NChannel3			= B15,
	.Break_In			= B12,
	.Mode				= COMPLEMENTARY
};

extern PWM_TypeDef GPWM2 = 
{
	.Timer				= TIMER2,
	.Channel1			= A0,
	.Channel2			= A1,
	.Channel3			= A2,
	.Channel4			= A3,
	.Mode				= SIMPLE
};

extern PWM_TypeDef GPWM3 = 
{
	.Timer				= TIMER3,
	.Channel1			= A6,
	.Channel2			= A7,
	.Channel3			= B0,
	.Channel4			= B1,
	.Mode				= SIMPLE
};

extern PWM_TypeDef GPWM4 = 
{
	.Timer				= TIMER4,
	.Channel1			= B6,
	.Channel2			= B7,
	.Channel3			= B8,
	.Channel4			= B9,
	.Mode				= SIMPLE
};