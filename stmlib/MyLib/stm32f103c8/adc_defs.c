#include "adc.h"

extern Adc_TypeDef GADC1 =
{
	.Adc			= ADC1,
	.CountChannels	= 0,
	.Channels		= {0},
	.RCC_Bus		= RCC_APB2Periph_ADC1,
	.RCC_Register	= &RCC->APB2ENR,
	.DmaChannel		= DMA_CHANNEL1,
	.AdcAddress		= &ADC1->DR
};