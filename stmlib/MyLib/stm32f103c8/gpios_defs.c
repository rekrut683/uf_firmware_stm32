#include "gpioss.h"

extern GPIOS_TypeDef GPORTA = 
{
	.GPIO			= GPIOA,
	.RCC_Bus		= RCC_APB2Periph_GPIOA,
	.RCC_Register	= &RCC->APB2ENR,
	.PortSource		= GPIO_PortSourceGPIOA
};

extern GPIOS_TypeDef GPORTB =
{
	.GPIO			= GPIOB,
	.RCC_Bus		= RCC_APB2Periph_GPIOB,
	.RCC_Register	= &RCC->APB2ENR,
	.PortSource		= GPIO_PortSourceGPIOB
};

extern GPIOS_TypeDef GPORTC =
{
	.GPIO			= GPIOC,
	.RCC_Bus		= RCC_APB2Periph_GPIOC,
	.RCC_Register	= &RCC->APB2ENR,
	.PortSource		= GPIO_PortSourceGPIOC
};

extern GPIOS_TypeDef GGPIO_AF = 
{
	.RCC_Bus		= RCC_APB2Periph_AFIO,
	.RCC_Register	= &RCC->APB2ENR
};