#ifndef _PINS_H
#define _PINS_H

#include "../platforms.h"

#if defined STM32F030
#include "../../StdPeriph/stm32f0xx/inc/stm32f0xx_gpio.h"
#elif defined STM32F10X_MD
#include "../../StdPeriph/stm32f10x/inc/stm32f10x_gpio.h"
#endif

#include "../gpio/gpioss.h"

#define A0		&PA0
#define A1		&PA1
#define A2		&PA2
#define A3		&PA3
#define A4		&PA4
#define A5		&PA5
#define A6		&PA6
#define A7		&PA7
#define A8		&PA8
#define A9		&PA9
#define A10		&PA10
#define A11		&PA11
#define A12		&PA12
#define A13		&PA13
#define A14		&PA14
#define A15		&PA15
#define B0		&PB0
#define B1		&PB1
#define B2		&PB2
#define B3		&PB3
#define B4		&PB4
#define B5		&PB5
#define B6		&PB6
#define B7		&PB7
#define B8		&PB8
#define B9		&PB9
#define B10		&PB10
#define B11		&PB11
#define B12		&PB12
#define B13		&PB13
#define B14		&PB14
#define B15		&PB15
#define C13		&PC13

/*
//	TypeDef structs for gpio pin's
*/
typedef enum Pins_speed_enum
{
	LOW_SPEED 			= GPIO_Speed_2MHz,
	MIDDLE_SPEED 		= GPIO_Speed_10MHz,
	HIGH_SPEED 			= GPIO_Speed_50MHz
} Pins_speed;

#if defined STM32F10X_MD
typedef enum Pins_mode_union
{
  	ANALOG_INPUT				= GPIO_Mode_AIN,
	THIRD_STATE					= GPIO_Mode_IN_FLOATING,
	INPUT_PULL_DOWN				= GPIO_Mode_IPD,
	INPUT_PULL_UP				= GPIO_Mode_IPU,
	OUTPUT_PULL_DOWN			= GPIO_Mode_Out_OD,
	OUTPUT_PUSH_PULL			= GPIO_Mode_Out_PP,
	ALTERNATE_FUNC_DOWN			= GPIO_Mode_AF_OD,
	ALTERNATE_FUNC_PUSH_PULL	= GPIO_Mode_AF_PP
} Pins_mode;
#elif defined STM32F030
typedef enum Pins_mode_union
{
	ANALOG_INPUT				= GPIO_Mode_AN,
	INPUT_MODE					= GPIO_Mode_IN,
	OUTPUT_MODE					= GPIO_Mode_OUT,
	ALTERNATE_FUNC_MODE			= GPIO_Mode_AF
} Pins_mode;
#endif

#if defined STM32F030
typedef enum Pins_PuPd_enum
{
	PUPD_THIRD_STATE			= GPIO_PuPd_NOPULL,
	PUPD_UP						= GPIO_PuPd_UP,
	PUPD_DOWN					= GPIO_PuPd_DOWN
} Pins_PuPd;
typedef enum Pins_OType_enum
{
	OTYPE_PUSHPULL				= GPIO_OType_PP,
	OTYPE_OPENDRAIN				= GPIO_OType_OD
} Pins_OType;
#endif

#if defined STM32F030
typedef enum Pins_AF_mode_enum
{
	SPI_TIM_USART_1				= GPIO_AF_0,			/* WKUP, EVENTOUT, TIM15, SPI1, TIM17,
                                                		MCO, SWDAT, SWCLK, TIM14, BOOT,
                                                		USART1, CEC, IR_OUT, SPI2, TS, TIM3,
                                                		USART4, CAN, TIM3, USART2, USART3, 
                                                		CRS, TIM16, TIM1 */
	SPI_TIM_USART_2				= GPIO_AF_1,			/* USART2, CEC, TIM3, USART1, IR,
                                                		EVENTOUT, I2C1, I2C2, TIM15, SPI2,
                                                		USART3, TS, SPI1 */
	TIM_EVENT_USB				= GPIO_AF_2,			/* TIM2, TIM1, EVENTOUT, TIM16, TIM17,
														USB */
	TIM_EVENT_I2C				= GPIO_AF_3,			/* TS, I2C1, TIM15, EVENTOUT */
	TIM_USART_I2C				= GPIO_AF_4,			/* TIM14, USART4, USART3, CRS, CAN,
                                                		I2C1 */
	TIM_SPI_USB					= GPIO_AF_5,			/* TIM16, TIM17, TIM15, SPI2, I2C2, 
                                                		MCO, I2C1, USB */
	EVENT						= GPIO_AF_6,			/* EVENTOUT */
	COMP1_COMP2					= GPIO_AF_7				/* COMP1 OUT and COMP2 OUT */
} Pins_AF;
#endif

typedef struct Pins_struct
{
	GPIOS_TypeDef* 	Port;
	uint32_t		Pin;
	Pins_speed		Speed;
	Pins_mode		Mode;
#if defined STM32F030
	Pins_PuPd		PuPd;
	Pins_OType		OType;
#endif
	uint8_t			PinSource;
	uint8_t			IsAdcPin;
	uint8_t			AdcChannel;
} Pins_TypeDef;


/*
//	Public methods for gpio pin's
*/
#if defined STM32F10X_MD
void Pin_Init(Pins_TypeDef* pin, Pins_mode mode, Pins_speed speed);
#elif defined STM32F030
void Pin_Init(Pins_TypeDef* pin, Pins_mode mode, Pins_speed speed, Pins_PuPd pupd, Pins_OType otype);
#endif

void Pin_Set(Pins_TypeDef* pin);
void Pin_Clear(Pins_TypeDef* pin);
void Pin_Toggle(Pins_TypeDef* pin);

uint8_t Pin_Read(Pins_TypeDef* pin);


/*
//	static instances for pin's Stm32f103/Stm32f030
*/
extern Pins_TypeDef PA0;
extern Pins_TypeDef PA1;
extern Pins_TypeDef PA2;
extern Pins_TypeDef PA3;
extern Pins_TypeDef PA4;
extern Pins_TypeDef PA5;
extern Pins_TypeDef PA6;
extern Pins_TypeDef PA7;
extern Pins_TypeDef PA8;
extern Pins_TypeDef PA9;
extern Pins_TypeDef PA10;
extern Pins_TypeDef PA11;
extern Pins_TypeDef PA12;
extern Pins_TypeDef PA13;
extern Pins_TypeDef PA14;
extern Pins_TypeDef PA15;
extern Pins_TypeDef PB0;
extern Pins_TypeDef PB1;
extern Pins_TypeDef PB2;
extern Pins_TypeDef PB3;
extern Pins_TypeDef PB4;
extern Pins_TypeDef PB5;
extern Pins_TypeDef PB6;
extern Pins_TypeDef PB7;
extern Pins_TypeDef PB8;
extern Pins_TypeDef PB9;
extern Pins_TypeDef PB10;
extern Pins_TypeDef PB11;
extern Pins_TypeDef PB12;
extern Pins_TypeDef PB13;
extern Pins_TypeDef PB14;
extern Pins_TypeDef PB15;
extern Pins_TypeDef PC13;

#endif