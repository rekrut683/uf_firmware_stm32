#include "pins.h"

#if defined STM32F10X_MD
void Pin_Init(Pins_TypeDef* pin, Pins_mode mode, Pins_speed speed)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = pin->Pin;
	pin->Mode = mode;
	GPIO_InitStructure.GPIO_Mode = pin->Mode;
	pin->Speed = speed;
	GPIO_InitStructure.GPIO_Speed = pin->Speed;
	GPIO_Init(pin->Port->GPIO, &GPIO_InitStructure);
}
#elif defined STM32F030
void Pin_Init(Pins_TypeDef* pin, Pins_mode mode, Pins_speed speed, Pins_PuPd pupd, Pins_OType otype)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = pin->Pin;
	pin->Mode = mode;
	pin->PuPd = pupd;
	pin->OType = otype;
	GPIO_InitStructure.GPIO_Mode = pin->Mode;
	GPIO_InitStructure.GPIO_PuPd = pin->PuPd;
	GPIO_InitStructure.GPIO_OType = pin->OType;
	pin->Speed = speed;
	GPIO_InitStructure.GPIO_Speed = pin->Speed;
	GPIO_Init(pin->Port->GPIO, &GPIO_InitStructure);
}
#endif

void Pin_Set(Pins_TypeDef* pin)
{
	GPIO_SetBits(pin->Port->GPIO, pin->Pin);
}

void Pin_Clear(Pins_TypeDef* pin)
{
	GPIO_ResetBits(pin->Port->GPIO, pin->Pin);
}

void Pin_Toggle(Pins_TypeDef* pin)
{
	uint8_t read = GPIO_ReadInputDataBit(pin->Port->GPIO, pin->Pin);
	if(!read)
	{
		Pin_Set(pin);
	}
	else
	{
		Pin_Clear(pin);
	}
}

uint8_t Pin_Read(Pins_TypeDef* pin)
{
	return GPIO_ReadInputDataBit(pin->Port->GPIO, pin->Pin);
}