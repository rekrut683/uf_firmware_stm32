#ifndef _EXTI_H
#define _EXTI_H

#include "../platforms.h"

#if defined STM32F10X_MD
#include "../../CMSIS/stm32f10x/stm32f10x.h"
#elif defined STM32F030
#include "../../CMSIS/stm32f0xx/stm32f0xx.h"
#endif

#if defined STM32F10X_MD
#include "../../StdPeriph/stm32f10x/inc/stm32f10x_exti.h"
#include "../../StdPeriph/stm32f10x/inc/stm32f10x_gpio.h"
#elif defined STM32F030
#include "../../StdPeriph/stm32f0xx/inc/stm32f0xx_exti.h"
#include "../../StdPeriph/stm32f0xx/inc/stm32f0xx_gpio.h"
#endif

#include "../pin/pins.h"

#define EXTI0			&GEXTI_LINE0
#define EXTI1			&GEXTI_LINE1
#define EXTI2			&GEXTI_LINE2
#define EXTI3			&GEXTI_LINE3
#define EXTI4			&GEXTI_LINE4
#define EXTI5			&GEXTI_LINE5
#define EXTI6			&GEXTI_LINE6
#define EXTI7			&GEXTI_LINE7
#define EXTI8			&GEXTI_LINE8
#define EXTI9			&GEXTI_LINE9
#define EXTI10			&GEXTI_LINE10
#define EXTI11			&GEXTI_LINE11
#define EXTI12			&GEXTI_LINE12
#define EXTI13			&GEXTI_LINE13
#define EXTI14			&GEXTI_LINE14
#define EXTI15			&GEXTI_LINE15

#define EXTI_CALLBACK_COUNT			3

typedef void (*ExtiIRQHandler)(void* context);

/*
	typedef enums and structs for exti
*/
typedef enum Exti_trigger_enum
{
	RISING = EXTI_Trigger_Rising,
	FALLING = EXTI_Trigger_Falling,
	RISING_FALLING = EXTI_Trigger_Rising_Falling
} Exti_trigger;

typedef enum Exti_mode_enum
{
	EXTI_Interrupt = EXTI_Mode_Interrupt,
	EXTI_Event = EXTI_Mode_Event
} Exti_mode;

typedef struct Exti_struct
{
	uint32_t				ExtiLine;
	ExtiIRQHandler			Callbacks[EXTI_CALLBACK_COUNT];
	uint8_t					CallbackCounter;
	void*					Context;
	Exti_trigger			Trigger;
	Exti_mode				ExtiMode;
} Exti_TypeDef;

void Exti_Init(Exti_TypeDef* exti, Pins_TypeDef* pin, void (*callback)(void* context), void* context);
void Exti_DeInit(Exti_TypeDef* exti);
void Exti_On(Exti_TypeDef* exti, Exti_trigger trigger, Exti_mode mode, uint8_t priority);
void Exti_Off(Exti_TypeDef* exti);

/*
	//	Private methods
*/
IRQn_Type GetIRQForExti(Exti_TypeDef* exti);

/*
	extern instances of TIMERS
*/
extern Exti_TypeDef GEXTI_LINE0;
extern Exti_TypeDef GEXTI_LINE1;
extern Exti_TypeDef GEXTI_LINE2;
extern Exti_TypeDef GEXTI_LINE3;
extern Exti_TypeDef GEXTI_LINE4;
extern Exti_TypeDef GEXTI_LINE5;
extern Exti_TypeDef GEXTI_LINE6;
extern Exti_TypeDef GEXTI_LINE7;
extern Exti_TypeDef GEXTI_LINE8;
extern Exti_TypeDef GEXTI_LINE9;
extern Exti_TypeDef GEXTI_LINE10;
extern Exti_TypeDef GEXTI_LINE11;
extern Exti_TypeDef GEXTI_LINE12;
extern Exti_TypeDef GEXTI_LINE13;
extern Exti_TypeDef GEXTI_LINE14;
extern Exti_TypeDef GEXTI_LINE15;

#endif