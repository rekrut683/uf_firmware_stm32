#include "exti.h"
#include "../nvic/nvic.h"


#if defined STM32F10X_MD
void EXTI0_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line0);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI0).Callbacks[i])
			{
				((*EXTI0).Callbacks[i])((*EXTI0).Context);
			}
		}
	}
}
void EXTI1_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line1) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line1);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI1).Callbacks[i])
			{
				((*EXTI1).Callbacks[i])((*EXTI1).Context);
			}
		}
	}
}
void EXTI2_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line2) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line2);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI2).Callbacks[i])
			{
				((*EXTI2).Callbacks[i])((*EXTI2).Context);
			}
		}
	}
}
void EXTI3_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line3) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line3);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI3).Callbacks[i])
			{
				((*EXTI3).Callbacks[i])((*EXTI3).Context);
			}
		}
	}
}
void EXTI4_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line4) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line4);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI4).Callbacks[i])
			{
				((*EXTI4).Callbacks[i])((*EXTI4).Context);
			}
		}
	}
}
void EXTI9_5_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line5) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line5);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI5).Callbacks[i])
			{
				((*EXTI5).Callbacks[i])((*EXTI5).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line6) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line6);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI6).Callbacks[i])
			{
				((*EXTI6).Callbacks[i])((*EXTI6).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line7) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line7);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI7).Callbacks[i])
			{
				((*EXTI7).Callbacks[i])((*EXTI7).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line8) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line8);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI8).Callbacks[i])
			{
				((*EXTI8).Callbacks[i])((*EXTI8).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line9) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line9);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI9).Callbacks[i])
			{
				((*EXTI9).Callbacks[i])((*EXTI9).Context);
			}
		}
	}
}
void EXTI15_10_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line10) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line10);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI10).Callbacks[i])
			{
				((*EXTI10).Callbacks[i])((*EXTI10).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line11) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line11);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI11).Callbacks[i])
			{
				((*EXTI11).Callbacks[i])((*EXTI11).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line12) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line12);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI12).Callbacks[i])
			{
				((*EXTI12).Callbacks[i])((*EXTI12).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line13) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line13);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI13).Callbacks[i])
			{
				((*EXTI13).Callbacks[i])((*EXTI13).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line14) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line14);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI14).Callbacks[i])
			{
				((*EXTI14).Callbacks[i])((*EXTI14).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line15) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line15);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI15).Callbacks[i])
			{
				((*EXTI15).Callbacks[i])((*EXTI15).Context);
			}
		}
	}
}
#elif defined STM32F030
void EXTI0_1_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line0);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI0).Callbacks[i])
			{
				((*EXTI0).Callbacks[i])((*EXTI0).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line1) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line1);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI1).Callbacks[i])
			{
				((*EXTI1).Callbacks[i])((*EXTI1).Context);
			}
		}
	}
}
void EXTI2_3_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line2) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line2);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI2).Callbacks[i])
			{
				((*EXTI2).Callbacks[i])((*EXTI2).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line3) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line3);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI3).Callbacks[i])
			{
				((*EXTI3).Callbacks[i])((*EXTI3).Context);
			}
		}
	}
}
void EXTI4_15_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line4) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line4);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI4).Callbacks[i])
			{
				((*EXTI4).Callbacks[i])((*EXTI4).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line5) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line5);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI5).Callbacks[i])
			{
				((*EXTI5).Callbacks[i])((*EXTI5).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line6) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line6);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI6).Callbacks[i])
			{
				((*EXTI6).Callbacks[i])((*EXTI6).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line7) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line7);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI7).Callbacks[i])
			{
				((*EXTI7).Callbacks[i])((*EXTI7).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line8) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line8);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI8).Callbacks[i])
			{
				((*EXTI8).Callbacks[i])((*EXTI8).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line9) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line9);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI9).Callbacks[i])
			{
				((*EXTI9).Callbacks[i])((*EXTI9).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line10) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line10);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI10).Callbacks[i])
			{
				((*EXTI10).Callbacks[i])((*EXTI10).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line11) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line11);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI11).Callbacks[i])
			{
				((*EXTI11).Callbacks[i])((*EXTI11).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line12) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line12);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI12).Callbacks[i])
			{
				((*EXTI12).Callbacks[i])((*EXTI12).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line13) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line13);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI13).Callbacks[i])
			{
				((*EXTI13).Callbacks[i])((*EXTI13).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line14) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line14);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI14).Callbacks[i])
			{
				((*EXTI14).Callbacks[i])((*EXTI14).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line15) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line15);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI15).Callbacks[i])
			{
				((*EXTI15).Callbacks[i])((*EXTI15).Context);
			}
		}
	}
}
#endif

void Exti_Init(Exti_TypeDef* exti, Pins_TypeDef* pin, void (*callback)(void* context), void* context)
{
#if defined STM32F030
	if ((RCC->APB2ENR & 1) == 0)
	{
		RCC->APB2ENR |= RCC_APB2Periph_SYSCFG;
	}
#endif
	GPIOS_TypeDef* port = pin->Port;
	uint8_t portSource = port->PortSource;
#if defined STM32F10X_MD
	GPIO_EXTILineConfig(portSource, pin->PinSource);
#elif defined STM32F030
	SYSCFG_EXTILineConfig(port->PortSource, pin->PinSource);
#endif

	if (exti->CallbackCounter == EXTI_CALLBACK_COUNT)
	{
		exti->CallbackCounter = 0;
	}
	exti->Callbacks[exti->CallbackCounter] = callback;
	exti->Context = context;
}

void Exti_DeInit(Exti_TypeDef* exti)
{

}

void Exti_On(Exti_TypeDef* exti, Exti_trigger trigger, Exti_mode mode, uint8_t priority)
{
	exti->Trigger = trigger;
	exti->ExtiMode = mode;
	
	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line = exti->ExtiLine;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = exti->ExtiMode;
	EXTI_InitStruct.EXTI_Trigger = exti->Trigger;
	EXTI_Init(&EXTI_InitStruct);

	uint8_t irq = GetIRQForExti(exti);
	
	if(!NVIC_Check(irq))
	{
		NVIC_Add(irq);
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = irq;
#if defined STM32F10X_MD
		NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
#elif defined STM32F030
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
#endif
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
}

void Exti_Off(Exti_TypeDef* exti)
{
	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line = exti->ExtiLine;
	EXTI_InitStruct.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStruct);
}


/******************PRIVATE METHODS*******************/
/*
	Get IRQ Type for NVIC
*/
IRQn_Type GetIRQForExti(Exti_TypeDef* exti)
{
	if (exti->ExtiLine == EXTI_Line0)
	{
#if defined STM32F10X_MD
		return EXTI0_IRQn;
#elif defined STM32F030
		return EXTI0_1_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line1)
	{
#if defined STM32F10X_MD
		return EXTI1_IRQn;
#elif defined STM32F030
		return EXTI0_1_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line2)
	{
#if defined STM32F10X_MD
		return EXTI2_IRQn;
#elif defined STM32F030
		return EXTI2_3_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line3)
	{
#if defined STM32F10X_MD
		return EXTI3_IRQn;
#elif defined STM32F030
		return EXTI2_3_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line4)
	{
#if defined STM32F10X_MD
		return EXTI4_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line5)
	{
#if defined STM32F10X_MD
		return EXTI9_5_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line6)
	{
#if defined STM32F10X_MD
		return EXTI9_5_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line7)
	{
#if defined STM32F10X_MD
		return EXTI9_5_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line8)
	{
#if defined STM32F10X_MD
		return EXTI9_5_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line9)
	{
#if defined STM32F10X_MD
		return EXTI9_5_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line10)
	{
#if defined STM32F10X_MD
		return EXTI15_10_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line11)
	{
#if defined STM32F10X_MD
		return EXTI15_10_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line12)
	{
#if defined STM32F10X_MD
		return EXTI15_10_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line13)
	{
#if defined STM32F10X_MD
		return EXTI15_10_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line14)
	{
#if defined STM32F10X_MD
		return EXTI15_10_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else if (exti->ExtiLine == EXTI_Line15)
	{
#if defined STM32F10X_MD
		return EXTI15_10_IRQn;
#elif defined STM32F030
		return EXTI4_15_IRQn;
#endif
	}
	else
	{
		return -25;
	}
}