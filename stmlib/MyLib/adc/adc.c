#include "adc.h"

/*
uint16_t Adc_GetValue(Adc_TypeDef* adc)
{
	while (ADC_GetFlagStatus(adc->Adc, ADC_FLAG_EOC) == RESET) {}
	return ADC_GetConversionValue(adc->Adc);
}

void Adc_SetChannel(Adc_TypeDef* adc, uint8_t number, Pins_TypeDef* pin, uint8_t samples)
{
	if (!pin->IsAdcPin && number != 0 && number < MAX_ADC_CHANNELS)
	{
		return;
	}
	uint8_t channel = pin->AdcChannel;
	Adc_Channel_TypeDef adcChannel =
	{
		.Number = number,
		.Ain = channel
	};
	adc->Channels[number - 1] = adcChannel;
	ADC_RegularChannelConfig(adc->Adc, channel, number, samples);
}

void Adc_Init(Adc_TypeDef* adc, uint32_t divider, uint8_t numberChannels)
{
	RCC_ADCCLKConfig(divider);
	*adc->RCC_Register |= adc->RCC_Bus;

	adc->CountChannels = numberChannels;

	ADC_InitTypeDef ADC_InitStructure;

	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_NbrOfChannel = adc->CountChannels;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_Init(adc->Adc, &ADC_InitStructure);
}

void Adc_Start(Adc_TypeDef* adc)
{
	ADC_Cmd(adc->Adc, ENABLE);
	ADC_ResetCalibration(adc->Adc);
	while (ADC_GetResetCalibrationStatus(adc->Adc));
	ADC_StartCalibration(adc->Adc);
	while (ADC_GetCalibrationStatus(adc->Adc));
	ADC_SoftwareStartConvCmd(adc->Adc, ENABLE);
}

void AdcDma_Init(Adc_TypeDef* adc, uint32_t bufferAdr, uint32_t bufferSize)
{
	DmaChannel_Init(adc->DmaChannel, 
					bufferSize, 
					(uint32_t)bufferAdr, 
					DMA_DIR_FROM_PERIPH, 
					DMA_MEMORY_SIZE_HALFWORD, 
					DMA_PERIPH_SIZE_HALFWORD, 
					DMA_MODE_CIRCULAR, 
					(uint32_t)adc->AdcAddress);
}

void AdcDma_Start(Adc_TypeDef* adc)
{
	ADC_DMACmd(adc->Adc, ENABLE);
}
*/