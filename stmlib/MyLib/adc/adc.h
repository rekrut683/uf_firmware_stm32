#ifndef _ADC_H
#define _ADC_H

#include "../platforms.h"

#if defined STM32F030
#include "../../CMSIS/stm32f0xx/stm32f0xx.h"
#include "../../StdPeriph/stm32f0xx/inc/stm32f0xx_adc.h"
#elif defined STM32F10X_MD
#include "../../CMSIS/stm32f10x/stm32f10x.h"
#include "../../StdPeriph/stm32f10x/inc/stm32f10x_adc.h"
#endif

#include <stdint.h>

/*
#include "../pin/pins.h"
#include "../dma/dma.h"



#define ADC_1		&GADC1

#define	MAX_ADC_CHANNELS		10

typedef struct ADC_Channel
{
	uint8_t		Number;
	uint8_t		Ain;
} Adc_Channel_TypeDef;

typedef struct ADC_Struct
{
	ADC_TypeDef*			Adc;
	uint8_t					CountChannels;
	Adc_Channel_TypeDef		Channels[MAX_ADC_CHANNELS];
	uint32_t	 			RCC_Bus;
	volatile uint32_t*		RCC_Register;
	DmaChannel_TypeDef*		DmaChannel;
	volatile uint32_t*		AdcAddress;
} Adc_TypeDef;

void Adc_Init(Adc_TypeDef* adc, uint32_t divider, uint8_t numberChannels);
void Adc_SetChannel(Adc_TypeDef* adc, uint8_t number, Pins_TypeDef* pin, uint8_t samples);
void Adc_Start(Adc_TypeDef* adc);

void AdcDma_Init(Adc_TypeDef* adc, uint32_t buffer, uint32_t bufferSize);
void AdcDma_Start(Adc_TypeDef* adc);

extern Adc_TypeDef GADC1;
*/

#endif