#include "uart.h"

#define USART_TX_BUFFERSIZE					32

/*
	Get IRQ Type for NVIC
*/
IRQn_Type GetIRQForUsart(Uart_TypeDef* uart)
{
	if (uart->Usart == USART1)
	{
		return USART1_IRQn;
	}
	else if (uart->Usart == USART2)
	{
		return USART2_IRQn;
	}
	else if (uart->Usart == USART3)
	{
		return USART3_IRQn;
	}
	else
	{
		return -25;
	}
}

void USART1_IRQHandler(void)
{
	if (((*USART_1).Usart->SR & USART_FLAG_RXNE) != RESET)
	{
		uint8_t rx = USART_ReceiveData((*USART_1).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_1).OnReceiveCallbacks[i].callback)
			{
				(*USART_1).OnReceiveCallbacks[i].callback((*USART_1).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
}

void USART2_IRQHandler(void)
{
	if (((*USART_2).Usart->SR & USART_FLAG_RXNE) != RESET)
	{
		uint8_t rx = USART_ReceiveData((*USART_2).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_2).OnReceiveCallbacks[i].callback)
			{
				(*USART_2).OnReceiveCallbacks[i].callback((*USART_2).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
}

void USART3_IRQHandler(void)
{
	if (((*USART_3).Usart->SR & USART_FLAG_RXNE) != RESET)
	{
		uint8_t rx = USART_ReceiveData((*USART_3).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_3).OnReceiveCallbacks[i].callback)
			{
				(*USART_3).OnReceiveCallbacks[i].callback((*USART_3).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
}

void Usart_Enable(Uart_TypeDef* uart)
{
	*uart->RCC_Register |= uart->RCC_Bus;
}

void Usart_Disable(Uart_TypeDef* uart)
{
	*uart->RCC_Register &= ~uart->RCC_Bus;
}

void Usart_DmaComplete(void* context)
{
	Uart_TypeDef* usart = (Uart_TypeDef*)context;
	usart->TxBusy = 0;
	USART_DMACmd(usart->Usart, USART_DMAReq_Tx, DISABLE);
	DmaChannel_Stop(usart->DmaTxChannel);
	Usart_ClearTxBuffer(usart);
}

void Usart_Init(Uart_TypeDef* usart, uint32_t baudrate)
{
	/* Configure the GPIOs */
	Pin_Init(usart->Tx_pin, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	Pin_Init(usart->Rx_pin, THIRD_STATE, HIGH_SPEED);

	/* Configure the USART1 */
	USART_InitTypeDef USART_InitStructure;
	
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(usart->Usart, &USART_InitStructure);
	USART_Cmd(usart->Usart, ENABLE);
	USART_ITConfig(usart->Usart, USART_IT_RXNE, ENABLE);
	
	//NVIC setup
	int irq_Ch = GetIRQForUsart(usart);
	if (irq_Ch != -25)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
		NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
	usart->TxBusy = 0;
}

void Usart_ClearTxBuffer(Uart_TypeDef* usart)
{
	for (int i = 0; i < USART_TX_BUFFER_SIZE; i++)
	{
		TxBuffer[i] = '\0';
	}
}

void Usart_SubscribeReceive(Uart_TypeDef* usart, void* context, void(*callback)(void*,uint8_t))
{
	usart->OnReceiveCallbacks->context = context;
	usart->OnReceiveCallbacks->callback = callback;
}

void Usart_Send(Uart_TypeDef* usart, const uint8_t* data)
{
	uint16_t count = 0;
	while(*(data+count))
	{
		while(USART_GetFlagStatus(usart->Usart, USART_FLAG_TC) == RESET);
		USART_SendData(usart->Usart, *(data+count));
		count++;
	}	
}

void UsartTxDma_Init(Uart_TypeDef* usart)
{
	/*DmaChannel_Init(usart->DmaTxChannel,
					USART_TX_BUFFERSIZE,
					(uint32_t)&TxBuffer[0],
					DMA_DIR_TO_PERIPH,
					DMA_MEMORY_SIZE_BYTE,
					DMA_PERIPH_SIZE_BYTE,
					DMA_MODE_NORMAL,
					(uint32_t)usart->UsartTxAddress);*/
	DmaChannel_SubscribeCompleteInterrupt(usart->DmaTxChannel, usart, Usart_DmaComplete);
}

void UsartTxDma_Send(Uart_TypeDef* usart, const uint8_t* data)
{
	while(usart->TxBusy);
	DmaChannel_Init(usart->DmaTxChannel,
					USART_TX_BUFFERSIZE,
					(uint32_t)&TxBuffer[0],
					DMA_DIR_TO_PERIPH,
					DMA_MEMORY_SIZE_BYTE,
					DMA_PERIPH_SIZE_BYTE,
					DMA_MODE_NORMAL,
					(uint32_t)usart->UsartTxAddress);
	DmaChannel_Start(usart->DmaTxChannel);
	uint16_t count = 0;
	while(*(data+count))
	{
		TxBuffer[count] = *(data+count);
		count++;
	}	
	USART_DMACmd(usart->Usart, USART_DMAReq_Tx, ENABLE);
	usart->TxBusy = 1;
}