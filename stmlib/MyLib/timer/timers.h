#ifndef _TIMERS_H
#define _TIMERS_H

#include "../platforms.h"

#if defined STM32F10X_MD
#include "../../StdPeriph/stm32f10x/inc/stm32f10x_rcc.h"
#include "../../StdPeriph/stm32f10x/inc/stm32f10x_tim.h"
#elif defined STM32F030
#include "../../StdPeriph/stm32f0xx/inc/stm32f0xx_rcc.h"
#include "../../StdPeriph/stm32f0xx/inc/stm32f0xx_tim.h"
#endif

/*
	defines TIMERS ins system STM32f103
*/
#define TIMER1		&GTIMER1
#if defined STM32F10X_MD
#define TIMER2		&GTIMER2
#define TIMER4		&GTIMER4
#endif
#define TIMER3		&GTIMER3
#if defined STM32F030
#define TIMER14		&GTIMER14
#define TIMER15		&GTIMER15
#define TIMER16		&GTIMER16
#define TIMER17		&GTIMER17
#endif


/*
	Prototype callbacks of timers interrupt
*/
typedef void (*TimerIRQHandler)(void* context);

/*
	typedef enums and structs for timers periph
*/
typedef enum Timer_mode_enum
{
	COUNTER_UP		= TIM_CounterMode_Up,
	COUNTER_DOWN	= TIM_CounterMode_Down,
	CENTER_ALIGN1	= TIM_CounterMode_CenterAligned1,
	CENTER_ALIGN2	= TIM_CounterMode_CenterAligned2,
	CENTER_ALIGN3	= TIM_CounterMode_CenterAligned3
} Timer_Mode;

typedef enum Timer_type_enum
{
	BASIC,
	GENERAL,
	ADVANCED
} Timer_Type;

typedef struct Timers_init_struct
{
	TIM_TypeDef* 		Timer;
	uint32_t			RCC_Bus;
	volatile uint32_t*	RCC_Register;
	uint32_t			Prescaler;
	uint32_t			Period;
	Timer_Mode			Mode;
	TimerIRQHandler		Callbacks[5];
#if defined STM32F030
	TimerIRQHandler		BrkUpCallback[5];
	TimerIRQHandler		CompareCallback[5];
#endif
	void*				Context;
	uint8_t				Count_callbacks;
	Timer_Type			Type;
	
} TIMER_TypeDef;

/*
	private methods
*/
static IRQn_Type GetIRQForTimer(TIMER_TypeDef* timer);

/*
	public methods
*/
void Timer_Enable(TIMER_TypeDef* timer);
void Timer_Disable(TIMER_TypeDef* timer);

void Timer_Init(TIMER_TypeDef* timer, uint32_t prescaler, uint32_t period, Timer_Mode mode);
void Timer_InitAdvanced(TIMER_TypeDef* timer, uint32_t prescaler, uint32_t period, Timer_Mode mode, uint32_t clock_division, uint32_t repCounter);
void Timer_Start(TIMER_TypeDef* timer);
void Timer_Stop(TIMER_TypeDef* timer);

void Timer_IRQEnable(TIMER_TypeDef* timer);
void Timer_IRQDisable(TIMER_TypeDef* timer);

void Timer_SetCounterValue(TIMER_TypeDef* timer, uint32_t value);
uint32_t Timer_GetCounterValue(TIMER_TypeDef* timer);
void Timer_SetPeriodValue(TIMER_TypeDef* timer, uint32_t value);

void Timer_IRQSubscribe(TIMER_TypeDef* timer, void (*callback)(void* context), void* context);
void Timer_IRQUnsubscribe(TIMER_TypeDef* timer, void (*callback)(void* context));

/*
	extern instances of TIMERS
*/
extern TIMER_TypeDef GTIMER1;
#if defined STM32F10X_MD
extern TIMER_TypeDef GTIMER2;
#endif
extern TIMER_TypeDef GTIMER3;
#if defined STM32F10X_MD
extern TIMER_TypeDef GTIMER4;
#endif
#if defined STM32F030
extern TIMER_TypeDef GTIMER14;
extern TIMER_TypeDef GTIMER15;
extern TIMER_TypeDef GTIMER16;
extern TIMER_TypeDef GTIMER17;
#endif
	
#endif