#include "timers.h"

/*
	Get IRQ Type for NVIC
*/
IRQn_Type GetIRQForTimer(TIMER_TypeDef* timer)
{ 
	if(timer->Timer == TIM1)
	{
#if defined STM32F10X_MD
		return TIM1_UP_IRQn;
#elif defined STM32F030
		return TIM1_BRK_UP_TRG_COM_IRQn;
#endif
	}
#if defined STM32F10X_MD
	else if(timer->Timer == TIM2)
	{
		return TIM2_IRQn;
	}
#endif
	else if(timer->Timer == TIM3)
	{
		return TIM3_IRQn;
	}
#if defined STM32F10X_MD
	else if(timer->Timer == TIM4)
	{
		return TIM4_IRQn;
	}
#endif
#if defined STM32F030
	else if (timer->Timer == TIM14)
	{
		return TIM14_IRQn;
	}
	else if (timer->Timer == TIM15)
	{
		return TIM15_IRQn;
	}
	else if (timer->Timer == TIM16)
	{
		return TIM16_IRQn;
	}
	else if (timer->Timer == TIM17)
	{
		return TIM17_IRQn;
	}
#endif
	else
	{
		return -25;
	}
}

/*
	Timers Interrupt Handlers
*/
#if defined STM32F10X_MD
void TIM1_UP_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
		for(uint8_t i = 0; i < (*TIMER1).Count_callbacks; i++)
		{
			((*TIMER1).Callbacks[i])((*TIMER1).Context);
		}
	}
}

void TIM2_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		for(uint8_t i = 0; i < (*TIMER2).Count_callbacks; i++)
		{
			((*TIMER2).Callbacks[i])((*TIMER2).Context);
		}
	}
}
#endif

#if defined STM32F030
void TIM1_BRK_UP_TRG_COM_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM1, TIM_IT_Break) != RESET ||
		TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET ||
		TIM_GetITStatus(TIM1, TIM_IT_Trigger) != RESET ||
		TIM_GetITStatus(TIM1, TIM_IT_COM) != RESET)
	{
		TIM_ClearITPendingBit(TIM1, TIM_IT_Break);
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
		TIM_ClearITPendingBit(TIM1, TIM_IT_Trigger);
		TIM_ClearITPendingBit(TIM1, TIM_IT_COM);
		for (uint8_t i = 0; i < (*TIMER1).Count_callbacks; i++)
		{
			((*TIMER1).Callbacks[i])((*TIMER1.Context));
		}
	}
}
/*void TIM1_CC_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM1, TIM_IT_CC1) != RESET)
	{
		TIM_ClearITPendingBit(TIM1, TIM_IT_CC1);
		for (uint8_t i = 0; i < (*TIMER1).Count_callbacks; i++)
		{
			((*TIMER1).CompareCallback[i])((*TIMER1.Context));
		}
	}
}*/
#endif

void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		for(uint8_t i = 0; i < (*TIMER3).Count_callbacks; i++)
		{
			((*TIMER3).Callbacks[i])((*TIMER3.Context));
		}
	}
}

#if defined STM32F10X_MD
void TIM4_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
		for(uint8_t i = 0; i < (*TIMER4).Count_callbacks; i++)
		{
			((*TIMER4).Callbacks[i])((*TIMER4.Context));
		}
	}
}
#endif

#if defined STM32F030
void TIM14_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM14, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
		for (uint8_t i = 0; i < (*TIMER14).Count_callbacks; i++)
		{
			((*TIMER14).Callbacks[i])((*TIMER14.Context));
		}
	}
}
void TIM15_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM15, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM15, TIM_IT_Update);
		for (uint8_t i = 0; i < (*TIMER15).Count_callbacks; i++)
		{
			((*TIMER15).Callbacks[i])((*TIMER15.Context));
		}
	}
}
void TIM16_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM16, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM16, TIM_IT_Update);
		for (uint8_t i = 0; i < (*TIMER16).Count_callbacks; i++)
		{
			((*TIMER16).Callbacks[i])((*TIMER16.Context));
		}
	}
}
void TIM17_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM17, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM17, TIM_IT_Update);
		for (uint8_t i = 0; i < (*TIMER17).Count_callbacks; i++)
		{
			((*TIMER17).Callbacks[i])((*TIMER17.Context));
		}
	}
}
#endif

/*
	Public methods for Timer Struct
*/
void Timer_Enable(TIMER_TypeDef* timer)
{
	*timer->RCC_Register |= timer->RCC_Bus;
}

void Timer_Disable(TIMER_TypeDef* timer)
{
	*timer->RCC_Register &= ~timer->RCC_Bus;
}

void Timer_Init(TIMER_TypeDef* timer, uint32_t prescaler, uint32_t period, Timer_Mode mode)
{	
	TIM_TimeBaseInitTypeDef TIMER_InitStructure;
	TIM_TimeBaseStructInit(&TIMER_InitStructure);
	
	timer->Mode = mode;
	TIMER_InitStructure.TIM_CounterMode = timer->Mode;
	
	timer->Prescaler = prescaler;
	TIMER_InitStructure.TIM_Prescaler = timer->Prescaler;
	
	timer->Period = period;
	TIMER_InitStructure.TIM_Period = period;
	
	TIM_TimeBaseInit(timer->Timer, &TIMER_InitStructure);
	
	
	
	//TIM_ITConfig(timer->Timer, TIM_IT_Update, ENABLE);
	//TIM_Cmd(TIM4, ENABLE);
}

void Timer_InitAdvanced(TIMER_TypeDef* timer, uint32_t prescaler, uint32_t period, Timer_Mode mode, uint32_t clock_division, uint32_t repCounter)
{
	TIM_TimeBaseInitTypeDef TIMER_InitStructure;
	//TIM_TimeBaseStructInit(&TIMER_InitStructure);
	
	timer->Mode = mode;
	TIMER_InitStructure.TIM_CounterMode = timer->Mode;
	
	timer->Prescaler = prescaler;
	TIMER_InitStructure.TIM_Prescaler = timer->Prescaler;
	
	timer->Period = period;
	TIMER_InitStructure.TIM_Period = period;

	TIMER_InitStructure.TIM_ClockDivision = clock_division;
	TIMER_InitStructure.TIM_RepetitionCounter = repCounter;
	
	TIM_TimeBaseInit(timer->Timer, &TIMER_InitStructure);
	
	//NVIC setup
	int irq_Ch = GetIRQForTimer(timer);
	if(irq_Ch != -25)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
#if defined STM32F10X_MD
		NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
#elif defined STM32F030
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
#endif
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
}

void Timer_Start(TIMER_TypeDef* timer)
{
	TIM_Cmd(timer->Timer, ENABLE);
	TIM_ITConfig(timer->Timer, TIM_IT_Update, ENABLE);
}

void Timer_Stop(TIMER_TypeDef* timer)
{
	TIM_Cmd(timer->Timer, DISABLE);
}

void Timer_SetCounterValue(TIMER_TypeDef* timer, uint32_t value)
{
	TIM_SetCounter(timer->Timer, value);
}

uint32_t Timer_GetCounterValue(TIMER_TypeDef* timer)
{
	return TIM_GetCounter(timer->Timer);
}

void Timer_SetPeriodValue(TIMER_TypeDef* timer, uint32_t value)
{
	timer->Timer->ARR = value;
}

void Timer_IRQSubscribe(TIMER_TypeDef* timer, void (*callback)(void* context), void* context)
{
	timer->Count_callbacks += 1;
	if(timer->Count_callbacks == 5)
	{
		timer->Count_callbacks = 4;
		return;
	}
	timer->Callbacks[timer->Count_callbacks-1] = callback;
	timer->Context = context;
}

void Timer_IRQUnsubscribe(TIMER_TypeDef* timer, void (*callback)(void* context))
{
	//������� ������� � ������� ���������� ��������
	int pos = -1;
	for(int i = 0; i < timer->Count_callbacks; i++)
	{
		if(callback == timer->Callbacks[i])
		{
			pos = i;
			break;
		}
	}
	//��� �� ���������� � ���-�� ���������� ��������� >0
	if(pos != -1 && timer->Count_callbacks)
	{
		//���� ��� �� ��������� � ������ �������, �� ����� ��������� ��� ���������
		if(timer->Count_callbacks > pos+1)
		{
			int count = pos+1 - timer->Count_callbacks;
			for(int i = pos; i < pos+count; i++)
			{
				timer->Callbacks[i] = timer->Callbacks[i+1];
			}
		}
	}
	// ���� ��������
}

void Timer_IRQEnable(TIMER_TypeDef* timer)
{
	//NVIC setup
	int irq_Ch = GetIRQForTimer(timer);
	if(irq_Ch != -25)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
#if defined STM32F10X_MD
		NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
#elif defined STM32F030
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
#endif
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
	TIM_ITConfig(timer->Timer, TIM_IT_Update, ENABLE);
}

void Timer_IRQDisable(TIMER_TypeDef* timer)
{
	//NVIC setup
	int irq_Ch = GetIRQForTimer(timer);
	if(irq_Ch != -25)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
		//NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		//NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelCmd = DISABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
	TIM_ITConfig(timer->Timer, TIM_IT_Update, DISABLE);
}