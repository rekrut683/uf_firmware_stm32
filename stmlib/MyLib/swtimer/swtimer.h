#ifndef _SWTIMER_H
#define _SWTIMER_H

#include <stdint.h>
#include "../systimer/systimer.h"

#define MAX_SWTIMERS				10

typedef void (*OnSWTimerCallbackType)(void*);

typedef struct SWTimer_struct
{
	void* Context;
	OnSWTimerCallbackType Callback;
	uint16_t InterruptValue;
	volatile uint64_t CurrentValue;
	volatile uint64_t StartValue;
	volatile uint8_t IsEnabled;
} SWTimer_TypeDef;

typedef struct SWTimerControl_struct
{
	SWTimer_TypeDef* SwTimers[MAX_SWTIMERS];
	uint8_t SwTimersCount;
} SWTimerControl_TypeDef;

void SWTimer_Interrupt();
static void SWTimer_AddToSysTimerList(SWTimer_TypeDef* swtimer);
static void SWTimer_DeleteFromSysTimerList(SWTimer_TypeDef* swtimer);

void SWTimer_Init(SWTimer_TypeDef* swtimer, uint16_t ticks);
void SWTimer_DeInit(SWTimer_TypeDef* swtimer);
void SWTimer_Start(SWTimer_TypeDef* swtimer);
void SWTimer_SubscribeCallback(SWTimer_TypeDef* swtimer, void* context, void (*callback)(void*));
void SWTimer_UnsubscribeCallback(SWTimer_TypeDef* swtimer);
void SWTimer_Stop(SWTimer_TypeDef* swtimer);
void SWTimer_Reset(SWTimer_TypeDef* swtimer);


#endif