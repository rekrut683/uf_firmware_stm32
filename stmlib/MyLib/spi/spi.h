#ifndef _SPI_H
#define _SPI_H

#include "../../CMSIS/stm32f10x.h"

#include "../../StdPeriph/inc/stm32f10x_spi.h"

#include "../pin/pins.h"

#include <stdint.h>

#define SPI_1				&GSPI1
#define SPI_2				&GSPI2

typedef void (*SpiReceiveHandler)(void);

typedef enum Spi_Direction_enum
{
	SPI_DIRECTION_TX			= SPI_Direction_1Line_Tx,
	SPI_DIRECTION_RX			= SPI_Direction_1Line_Rx,
	SPI_DIRECTION_FULL_DUPLEX	= SPI_Direction_2Lines_FullDuplex
} Spi_Direction;

typedef enum Spi_Mode_enum
{
	SPI_MASTER					= SPI_Mode_Master,
	SPI_SLAVE					= SPI_Mode_Slave
} Spi_Mode;

typedef enum Spi_Datasize_enum
{
	SPI_8b						= SPI_DataSize_8b,
	SPI_16b						= SPI_DataSize_16b
} Spi_Datasize;

typedef enum Spi_Prescaler_enum
{
	SPI_PRESCALER_2				= SPI_BaudRatePrescaler_2,
	SPI_PRESCALER_4				= SPI_BaudRatePrescaler_4,
	SPI_PRESCALER_8				= SPI_BaudRatePrescaler_8,
	SPI_PRESCALER_16			= SPI_BaudRatePrescaler_16,
	SPI_PRESCALER_32			= SPI_BaudRatePrescaler_32,
	SPI_PRESCALER_64			= SPI_BaudRatePrescaler_64,
	SPI_PRESCALER_128			= SPI_BaudRatePrescaler_128,
	SPI_PRESCALER_256			= SPI_BaudRatePrescaler_256
} Spi_Prescaler;

typedef enum Spi_FirstBit_enum
{
	SPI_MSB						= SPI_FirstBit_MSB,
	SPI_LSB						= SPI_FirstBit_LSB
} Spi_FirstBit;

typedef struct Spi_struct
{
	SPI_TypeDef*			Spi;
	Spi_Direction			Direction;
	Spi_Mode				Mode;
	Spi_Datasize			DataSize;
	Spi_Prescaler			Prescaler;
	Spi_FirstBit			FirstBit;
	Pins_TypeDef*			Nss;
	Pins_TypeDef*			Sck;
	Pins_TypeDef*			Mosi;
	Pins_TypeDef*			Miso;
	SpiReceiveHandler		SpiReceive;
	uint32_t	 			RCC_Bus;
	volatile uint32_t*		RCC_Register;
} Spi_TypeDef;

void Spi_Init(Spi_TypeDef* spi, Spi_Direction direction, Spi_Mode mode, Spi_Datasize datasize, Spi_Prescaler prescaler, Spi_FirstBit firstBit);
void Spi_DeInit(Spi_TypeDef* spi);
void Spi_Transmit(Spi_TypeDef* spi, uint16_t data);
void Spi_NssTransmit(Spi_TypeDef* spi);
void Spi_NssClear(Spi_TypeDef* spi);

extern Spi_TypeDef GSPI1;
extern Spi_TypeDef GSPI2;

#endif