#include "spi.h"

void Spi_Init(Spi_TypeDef* spi, Spi_Direction direction, Spi_Mode mode, Spi_Datasize datasize, Spi_Prescaler prescaler, Spi_FirstBit firstBit)
{
	*spi->RCC_Register |= spi->RCC_Bus;

	SPI_InitTypeDef SPI_InitStructure;
	Pin_Init(spi->Nss, OUTPUT_PUSH_PULL, HIGH_SPEED);
	Pin_Init(spi->Miso, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	Pin_Init(spi->Mosi, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	Pin_Init(spi->Sck, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);

	spi->Direction = direction;
	spi->Mode = mode;
	spi->DataSize = datasize;
	spi->Prescaler = prescaler;
	spi->FirstBit = firstBit;

	SPI_InitStructure.SPI_Direction			= spi->Direction;
	SPI_InitStructure.SPI_Mode				= spi->Mode;
	SPI_InitStructure.SPI_DataSize			= spi->DataSize;
	SPI_InitStructure.SPI_CPOL				= SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA				= SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS				= SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = spi->Prescaler;
	SPI_InitStructure.SPI_FirstBit			= spi->FirstBit;

	SPI_Init(spi->Spi, &SPI_InitStructure);
	SPI_Cmd(spi->Spi, ENABLE);
}

void Spi_Transmit(Spi_TypeDef* spi, uint16_t data)
{
	while (SPI_I2S_GetFlagStatus(spi->Spi, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(spi->Spi, data);
}

void Spi_NssTransmit(Spi_TypeDef* spi)
{
	Pin_Set(spi->Nss);
}
void Spi_NssClear(Spi_TypeDef* spi)
{
	Pin_Clear(spi->Nss);
}