//#define STM32F030

#include "../stmlib/MyLib/gpio/gpioss.h"
#include "../stmlib/MyLib/pin/pins.h"
#include "../stmlib/MyLib/systimer/systimer.h"
#include "../stmlib/MyLib/exti/exti.h"
#include "../stmlib/MyLib/timer/timers.h"

#include "../Libs/LiquidCrystal/LiquidCrystal.h"

void TestLedInterrupt(void* context)
{
	//Pin_Toggle(B2);
}


void ExtiCallback(void* context)
{
	Pin_Toggle(A5);
}
int aa = 5;
LCD1602 lcd;

void main()
{
	// �������� ���������� ��������� �� 8 ���
	RCC_HSICmd(ENABLE);						
	// ������ ����� �� ��������� ����
	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);

	// ��������� ������
	SysTimer_Start(SYSTICK_1MS);

	// �������� �����
	Port_Enable(PORTA);
	Port_Enable(PORTB);
	Port_Enable(PORTC);

	// ���������
	Pin_Init(A5, OUTPUT_MODE, HIGH_SPEED, PUPD_UP, OTYPE_PUSHPULL);
	Pin_Clear(A5);
	
	//Pin_Init(B2, OUTPUT_MODE, HIGH_SPEED, PUPD_UP, OTYPE_PUSHPULL);
	//Pin_Clear(B2);
	
	
	SWTimer_TypeDef LedTimer;
	SWTimer_Init(&LedTimer, 1000);
	SWTimer_SubscribeCallback(&LedTimer, 0, TestLedInterrupt);
	SWTimer_Start(&LedTimer);

	//Exti_Init(EXTI2, B2, ExtiCallback, 0);
	//Exti_On(EXTI2, RISING_FALLING, EXTI_Interrupt, 0);

	/*Timer_Enable(TIMER14);
	Timer_Init(TIMER14, 7200, 1000, COUNTER_UP);
	Timer_IRQSubscribe(TIMER14, ExtiCallback, 0);
	Timer_IRQEnable(TIMER14);
	Timer_Start(TIMER14);*/
	
	LiquidCrystal_Init(&lcd, A0, A1, A4, B0, A10, B3);
	//LiquidCrystal_Begin(&lcd, 16, 2);

	SysTimer_DelayMs(300);
	
	//LiquidCrystal_clear(&lcd);
	LiquidCrystal_Print(&lcd, "Device on");
	//LiquidCrystal_blink(&lcd);
	//SysTimer_DelayMs(1);
	//LiquidCrystal_cursor(&lcd);
	//SysTimer_DelayMs(1);
	//LiquidCrystal_display(&lcd);
	//LiquidCrystal_write(&lcd, 1);

	while(1)
	{
		
	}
	
}